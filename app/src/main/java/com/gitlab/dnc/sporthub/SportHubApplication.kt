package com.gitlab.dnc.sporthub

import android.app.Application

class SportHubApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        instance = this
    }

    companion object {
        lateinit var instance: SportHubApplication
            private set
    }
}