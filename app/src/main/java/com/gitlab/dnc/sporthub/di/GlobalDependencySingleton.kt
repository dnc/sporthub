package com.gitlab.dnc.sporthub.di

import android.content.Context
import android.net.ConnectivityManager
import com.gitlab.dnc.sporthub.SportHubApplication
import com.gitlab.dnc.sporthub.util.net.NetworkManager
import com.gitlab.dnc.sporthub.util.notifications.NotificationManager

object GlobalDependencySingleton {
    val applicationContext by lazy { SportHubApplication.instance }

    val applicationPackage by lazy { applicationContext.packageName }

    val notificationManager by lazy { NotificationManager(applicationContext) }

    val connectivityManager by lazy { applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    val networkManager by lazy { NetworkManager(applicationContext) }
}