package com.gitlab.dnc.sporthub.event

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.util.extensions.inflate
import kotlinx.android.synthetic.main.activity_sport_events_sport_event_list_item.view.*

class SportEventListAdapter(sportEvents: List<SportEvent>) : RecyclerView.Adapter<SportEventListAdapter.SportEventViewHolder>() {
    private val sportEvents = sportEvents.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportEventViewHolder {
        return SportEventViewHolder(parent.inflate(R.layout.activity_sport_events_sport_event_list_item))
    }

    override fun getItemCount() = sportEvents.size

    override fun onBindViewHolder(viewHolder: SportEventViewHolder, position: Int) {
        viewHolder.bindWith(sportEvents[position])
    }

    fun updateSportEvents(sportEvents: List<SportEvent>) {
        synchronized(this.sportEvents) {
            this.sportEvents.clear()
            this.sportEvents.addAll(sportEvents)

            notifyDataSetChanged()
        }
    }

    class SportEventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindWith(sportEvent: SportEvent) {
            itemView.tvSportEventName.text = sportEvent.name
        }
    }
}