package com.gitlab.dnc.sporthub.event

enum class SportEventSearchStatus {
    IN_PROGRESS,
    READ_RAM,
    READ_DISK,
    READ_HTTP,
    DONE
}