package com.gitlab.dnc.sporthub.event

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.di.GlobalDependencySingleton.networkManager
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.event.model.di.DependencySingletons.sportEventRepository
import com.gitlab.dnc.sporthub.util.extensions.content
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_sport_events.*
import java.util.concurrent.TimeUnit

class SportEventsActivity : AppCompatActivity() {
    private lateinit var viewModel: SportEventsViewModel

    private val rxObservables by lazy { CompositeDisposable() }

    private val blinkingAnimation by lazy { AnimationUtils.loadAnimation(this, R.anim.blink) }

    private var deviceHasInternetAccess = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sport_events)

        viewModel = ViewModelProviders.of(this, SportEventsViewModel.Factory(sportEventRepository, networkManager))
                .get(SportEventsViewModel::class.java)

        initSportEventRecyclerView()

        initObservers()
        initRxObservables()
    }

    override fun onResume() {
        super.onResume()

        retrieveAllSportEvents()
    }

    override fun onDestroy() {
        rxObservables.dispose()

        super.onDestroy()
    }

    private fun initSportEventRecyclerView() {
        rvSportEvents.layoutManager = LinearLayoutManager(this)
        rvSportEvents.adapter = SportEventListAdapter(emptyList())
    }

    private fun initObservers() {
        viewModel.matchingSearchEvents.observe(this, SportEventSearchResultObserver())

        viewModel.sportEventSearchStatus.observe(this, SportEventSearchStatusObserver())

        viewModel.internetAvailability.observe(this, InternetAvailabilityObserver())

        viewModel.sportEventSearchError.observe(this, Observer { handleSportEventSearchError(it) })
    }

    private fun initRxObservables() {
        rxObservables.addAll(
                editTextSearch.textChanges().skipInitialValue().debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { query -> performSportEventSearch(query) }
        )
    }

    private fun retrieveAllSportEvents() {
        viewModel.retrieveAllSportEvents(excludeHttpSource = !deviceHasInternetAccess)
    }

    private fun performSportEventSearch() {
        performSportEventSearch(editTextSearch.content)
    }

    private fun performSportEventSearch(sportEventNamePart: CharSequence) {
        viewModel.retrieveSportEventsByNamePart(sportEventNamePart.toString(), excludeHttpSource = !deviceHasInternetAccess)
    }

    private fun showStaleSportEventsBlinker() {
        searchStatusContainer.visibility = View.VISIBLE
        imgStaleSportEvents.visibility = View.VISIBLE
        imgStaleSportEvents.startAnimation(blinkingAnimation)

        imgSyncedSportEvents.clearAnimation()
        imgSyncedSportEvents.visibility = View.GONE
    }

    private fun showSyncedSportEventsBlinker() {
        searchStatusContainer.visibility = View.VISIBLE
        imgSyncedSportEvents.visibility = View.VISIBLE
        imgSyncedSportEvents.startAnimation(blinkingAnimation)

        imgStaleSportEvents.clearAnimation()
        imgStaleSportEvents.visibility = View.GONE
    }

    private fun showInternetAccessStatusContainer() {
        internetAccessStatusContainer.visibility = View.VISIBLE
        imgMissingInternetAccess.startAnimation(blinkingAnimation)
    }

    private fun hideInternetAccessStatusContainer() {
        imgMissingInternetAccess.clearAnimation()
        internetAccessStatusContainer.visibility = View.GONE
    }

    private fun hideSearchStatusContainerAsync() {
        Handler().postDelayed(::hideSearchStatusContainer, SYNCED_SPORT_EVENTS_BLINKER_TIMEOUT)
    }

    private fun hideSearchStatusContainer() {
        searchStatusContainer.visibility = View.GONE
    }

    private fun handleSportEventSearchError(throwable: Throwable?) {
        if (throwable == null) {
            return
        }

        AlertDialog.Builder(this).apply {
            setTitle(R.string.sport_events_search_error_dialog_title)
            setMessage(R.string.sport_events_search_error_dialog_content)
            setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.cancel() }
        }.show()
    }

    companion object {
        /**
         * Value in milliseconds
         */
        private const val SYNCED_SPORT_EVENTS_BLINKER_TIMEOUT = 2000L
    }

    private inner class SportEventSearchResultObserver : Observer<List<SportEvent>> {
        override fun onChanged(sportEvents: List<SportEvent>?) {
            sportEvents?.let {
                (rvSportEvents.adapter as SportEventListAdapter).updateSportEvents(sportEvents)

                if (sportEvents.isEmpty()) {
                    rvSportEvents.visibility = View.GONE
                    tvNoSportEventsPlaceholder.visibility = View.VISIBLE
                } else {
                    tvNoSportEventsPlaceholder.visibility = View.GONE
                    rvSportEvents.visibility = View.VISIBLE
                }
            }
        }
    }

    private inner class SportEventSearchStatusObserver : Observer<SportEventSearchStatus> {
        override fun onChanged(sportEventSearchStatus: SportEventSearchStatus?) {
            if (sportEventSearchStatus == null) {
                return
            }

            when (sportEventSearchStatus) {
                SportEventSearchStatus.IN_PROGRESS, SportEventSearchStatus.READ_RAM, SportEventSearchStatus.READ_DISK -> showStaleSportEventsBlinker()

                SportEventSearchStatus.READ_HTTP -> {
                    hideInternetAccessStatusContainer()
                    showSyncedSportEventsBlinker()
                }

                SportEventSearchStatus.DONE -> hideSearchStatusContainerAsync()
            }
        }
    }

    private inner class InternetAvailabilityObserver : Observer<Boolean> {
        override fun onChanged(isAvailable: Boolean?) {
            if (isAvailable == null) {
                return
            }

            deviceHasInternetAccess = isAvailable

            if (isAvailable) {
                performSportEventSearch()
                hideInternetAccessStatusContainer()
            } else {
                showInternetAccessStatusContainer()
            }
        }
    }
}
