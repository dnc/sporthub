package com.gitlab.dnc.sporthub.event

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.event.model.source.SportEventRepository
import com.gitlab.dnc.sporthub.event.model.source.SportEventSearchResult
import com.gitlab.dnc.sporthub.event.model.source.SportEventSourceType
import com.gitlab.dnc.sporthub.util.net.InternetAvailabilityCallback
import com.gitlab.dnc.sporthub.util.net.NetworkManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class SportEventsViewModel(private val sportEventRepository: SportEventRepository,
                           private val networkManager: NetworkManager) : ViewModel() {
    private val _matchingSearchEvents by lazy { MutableLiveData<List<SportEvent>>() }

    private val _sportEventSearchStatus by lazy { MutableLiveData<SportEventSearchStatus>() }

    private val _sportEventSearchError by lazy { MutableLiveData<Throwable>() }

    private val _internetAvailability by lazy { MutableLiveData<Boolean>() }

    private val rxObservables by lazy { CompositeDisposable() }

    private val monitoredNetworkTypes by lazy { arrayOf(NetworkManager.NetworkType.WIFI, NetworkManager.NetworkType.CELLULAR) }

    private val internetAvailabilityCallback by lazy { InternetAvailabilityCallback(networkManager) }

    init {
        initStartupInternetAvailabilityCallback()
        initInternetAvailabilityCallback()
    }

    /**
     * This method ensures that the internet availability is checked when the app is started. This method is necessary
     * because the callback registered in [initInternetAvailabilityCallback] won't be called unless the Internet
     * availability changes when the app is already active
     */
    private fun initStartupInternetAvailabilityCallback() {
        networkManager.hasInternetAccessAsync({ hasInternetAccess ->
            _internetAvailability.value = hasInternetAccess
        }, *monitoredNetworkTypes)
    }

    private fun initInternetAvailabilityCallback() {
        rxObservables.addAll(
                internetAvailabilityCallback.internetAvailabilityPublisher.subscribe { _internetAvailability.postValue(it) }
        )

        networkManager.registerInternetAvailabilityCallback(internetAvailabilityCallback, *monitoredNetworkTypes)
    }

    override fun onCleared() {
        rxObservables.clear()
        networkManager.unregisterInternetAvailabilityCallback(internetAvailabilityCallback)

        super.onCleared()
    }

    val matchingSearchEvents = _matchingSearchEvents

    val sportEventSearchStatus = _sportEventSearchStatus

    val internetAvailability = _internetAvailability

    val sportEventSearchError = _sportEventSearchError

    fun retrieveAllSportEvents(excludeHttpSource: Boolean) {
        retrieveSportEventsByNamePart("", excludeHttpSource)
    }

    fun retrieveSportEventsByNamePart(sportEventNamePart: String, excludeHttpSource: Boolean) {
        _sportEventSearchStatus.value = SportEventSearchStatus.IN_PROGRESS

        val searchResultObservable = sportEventRepository.retrieveSportEventsByNamePart(sportEventNamePart, excludeHttpSource)
                .subscribeOn(Schedulers.io())

        rxObservables.addAll(
                searchResultObservable.observeOn(AndroidSchedulers.mainThread()).subscribeWith(SportEventSearchResultObserver())
        )
    }

    private inner class SportEventSearchResultObserver : DisposableObserver<SportEventSearchResult>() {
        override fun onNext(sportEventSearchResult: SportEventSearchResult) {
            _matchingSearchEvents.value = sportEventSearchResult.sportEvents

            when (sportEventSearchResult.sportEventSourceType) {
                SportEventSourceType.RAM -> _sportEventSearchStatus.value = SportEventSearchStatus.READ_RAM
                SportEventSourceType.DISK -> _sportEventSearchStatus.value = SportEventSearchStatus.READ_DISK
                SportEventSourceType.HTTP -> _sportEventSearchStatus.value = SportEventSearchStatus.READ_HTTP
            }
        }

        override fun onComplete() {
            _sportEventSearchError.value = null
            _sportEventSearchStatus.value = SportEventSearchStatus.DONE
        }

        override fun onError(e: Throwable) {
            _sportEventSearchError.value = e
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val sportEventRepository: SportEventRepository,
                  private val networkManager: NetworkManager) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return SportEventsViewModel(sportEventRepository, networkManager) as T
        }
    }
}