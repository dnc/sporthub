package com.gitlab.dnc.sporthub.event.model.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gitlab.dnc.sporthub.event.model.db.dao.SportEventDao
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent

@Database(entities = [SportEvent::class], version = 1, exportSchema = false)
abstract class SportEventDatabase : RoomDatabase() {
    abstract fun getSportEventDao(): SportEventDao
}

class SportEventDatabaseInstanceHolder(private val application: Application) {
    val instance by lazy { Room.databaseBuilder(application, SportEventDatabase::class.java, "sport-event-database").build() }
}