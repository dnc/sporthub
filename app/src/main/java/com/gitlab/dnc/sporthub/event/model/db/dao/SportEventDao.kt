package com.gitlab.dnc.sporthub.event.model.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface SportEventDao {
    @Insert
    fun saveAllSportEvents(sportEvents: List<SportEvent>): Completable

    @Query("select * from SportEvent where name like '%' || :sportEventNamePart || '%'")
    fun getSportEventsByNamePart(sportEventNamePart: String): Single<List<SportEvent>>

    @Query("delete from SportEvent")
    fun deleteAllSportEvents(): Completable
}