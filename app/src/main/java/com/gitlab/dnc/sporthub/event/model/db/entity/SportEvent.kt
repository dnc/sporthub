package com.gitlab.dnc.sporthub.event.model.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class SportEvent(
        val name: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}