package com.gitlab.dnc.sporthub.event.model.di

import com.gitlab.dnc.sporthub.di.GlobalDependencySingleton
import com.gitlab.dnc.sporthub.event.model.db.SportEventDatabaseInstanceHolder
import com.gitlab.dnc.sporthub.event.model.source.SportEventRepository
import com.gitlab.dnc.sporthub.event.model.source.local.DiskSportEventSource
import com.gitlab.dnc.sporthub.event.model.source.local.RamSportEventSource
import com.gitlab.dnc.sporthub.event.model.source.remote.HttpSportEventSource

object DependencySingletons {
    val sportEventDatabase by lazy { SportEventDatabaseInstanceHolder(GlobalDependencySingleton.applicationContext).instance }

    val sportEventDao by lazy { sportEventDatabase.getSportEventDao() }

    val ramSportEventSource by lazy { RamSportEventSource() }

    val diskSportEventSource by lazy { DiskSportEventSource(sportEventDao) }

    val httpSportEventSource by lazy { HttpSportEventSource() }

    val sportEventRepository by lazy { SportEventRepository(ramSportEventSource, diskSportEventSource, httpSportEventSource) }
}