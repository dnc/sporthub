package com.gitlab.dnc.sporthub.event.model.source

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent

interface LocalSportEventSource : SportEventSource {
    fun cacheSportEvents(sportEvents: List<SportEvent>)
}