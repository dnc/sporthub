package com.gitlab.dnc.sporthub.event.model.source

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import io.reactivex.Observable
import io.reactivex.Single

class SportEventRepository(private val ramSportEventSource: LocalSportEventSource,
                           private val diskSportEventSource: LocalSportEventSource,
                           private val httpSportEventSource: SportEventSource) {

    fun retrieveSportEventsByNamePart(sportEventNamePart: String, excludeHttpSource: Boolean): Observable<SportEventSearchResult> {
        return Observable.concat(
                ramSportEventSource.getSportEventsByNamePart(sportEventNamePart).map { SportEventSearchResult(SportEventSourceType.RAM, it) }.toObservable(),
                diskSportEventSource.getSportEventsByNamePart(sportEventNamePart).map { SportEventSearchResult(SportEventSourceType.DISK, it) }.toObservable(),
                getSportEventsByNamePartFromHttpSource(sportEventNamePart).map { SportEventSearchResult(SportEventSourceType.HTTP, it) }.toObservable()
        ).take(if (excludeHttpSource) 2 else 3)
    }

    private fun getSportEventsByNamePartFromHttpSource(sportEventNamePart: String): Single<List<SportEvent>> {
        return httpSportEventSource.getSportEventsByNamePart(sportEventNamePart).doOnSuccess { sportEvents -> updateLocalSportEventSources(sportEvents) }
    }

    private fun updateLocalSportEventSources(sportEvents: List<SportEvent>) {
        ramSportEventSource.cacheSportEvents(sportEvents)
        diskSportEventSource.cacheSportEvents(sportEvents)
    }
}