package com.gitlab.dnc.sporthub.event.model.source

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent

data class SportEventSearchResult(val sportEventSourceType: SportEventSourceType, val sportEvents: List<SportEvent>)