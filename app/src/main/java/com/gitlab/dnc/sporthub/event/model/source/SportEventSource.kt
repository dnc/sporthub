package com.gitlab.dnc.sporthub.event.model.source

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import io.reactivex.Single

interface SportEventSource {
    fun getSportEventsByNamePart(sportEventNamePart: String): Single<List<SportEvent>>
}