package com.gitlab.dnc.sporthub.event.model.source

enum class SportEventSourceType {
    RAM,
    DISK,
    HTTP
}