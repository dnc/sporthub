package com.gitlab.dnc.sporthub.event.model.source.local

import android.annotation.SuppressLint
import com.gitlab.dnc.sporthub.event.model.db.dao.SportEventDao
import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.event.model.source.LocalSportEventSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")
class DiskSportEventSource(private val sportEventDao: SportEventDao) : LocalSportEventSource {
    override fun getSportEventsByNamePart(sportEventNamePart: String): Single<List<SportEvent>> {
        return sportEventDao.getSportEventsByNamePart(sportEventNamePart)
    }

    override fun cacheSportEvents(sportEvents: List<SportEvent>) {
        sportEventDao.deleteAllSportEvents().subscribeOn(Schedulers.io())
                .subscribe { sportEventDao.saveAllSportEvents(sportEvents).subscribe() }
    }
}