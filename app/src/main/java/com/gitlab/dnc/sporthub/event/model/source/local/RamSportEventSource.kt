package com.gitlab.dnc.sporthub.event.model.source.local

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.event.model.source.LocalSportEventSource
import io.reactivex.Single

class RamSportEventSource : LocalSportEventSource {
    private val sportEvents by lazy { mutableListOf<SportEvent>() }

    override fun getSportEventsByNamePart(sportEventNamePart: String): Single<List<SportEvent>> {
        synchronized(sportEvents) {
            return Single.just(sportEvents.filter { sportEvent -> sportEvent.name.contains(sportEventNamePart, ignoreCase = true) })
        }
    }

    override fun cacheSportEvents(sportEvents: List<SportEvent>) {
        synchronized(sportEvents) {
            this.sportEvents.clear()
            this.sportEvents.addAll(sportEvents)
        }
    }
}