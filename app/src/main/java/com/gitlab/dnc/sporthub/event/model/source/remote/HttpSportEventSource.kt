package com.gitlab.dnc.sporthub.event.model.source.remote

import com.gitlab.dnc.sporthub.event.model.db.entity.SportEvent
import com.gitlab.dnc.sporthub.event.model.source.SportEventSource
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class HttpSportEventSource : SportEventSource {
    override fun getSportEventsByNamePart(sportEventNamePart: String): Single<List<SportEvent>> {
        return Single.create<List<SportEvent>> { emitter ->
            Thread.sleep(2000)

            emitter.onSuccess(getSportEvents().filter { sportEvent -> sportEvent.name.contains(sportEventNamePart, ignoreCase = true) })
        }.subscribeOn(Schedulers.io())
    }

    private fun getSportEvents(): List<SportEvent> {
        return listOf(
                SportEvent("Cluj-Napoca Bike 2019"),
                SportEvent("Tennis festival 10"),
                SportEvent("International Yoga Day"),
                SportEvent("International Women's Golf Day"),
                SportEvent("Power Yoga"),
                SportEvent("Ping Pong masters")
        )
    }
}