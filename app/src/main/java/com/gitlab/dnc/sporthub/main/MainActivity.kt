package com.gitlab.dnc.sporthub.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.main.ui.dashboard.DashboardFragment
import com.gitlab.dnc.sporthub.main.ui.home.HomeFragment
import com.gitlab.dnc.sporthub.main.ui.notifications.NotificationsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var navigationController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        // bottomNavigationView.setOnNavigationItemSelectedListener(NavigationItemSelectedListener())

        navigationController = Navigation.findNavController(this, R.id.nav_host_fragment)

        bottomNavigationView.setupWithNavController(navigationController)

        NavigationUI.setupActionBarWithNavController(this, navigationController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navigationController.navigateUp() || super.onSupportNavigateUp()
    }

    private inner class NavigationItemSelectedListener : BottomNavigationView.OnNavigationItemSelectedListener {
        init {
            loadFragment(HomeFragment())
        }

        override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
            val fragment: Fragment = when (menuItem.itemId) {
                R.id.navigation_home -> HomeFragment()
                R.id.navigation_dashboard -> DashboardFragment()
                R.id.navigation_notifications -> NotificationsFragment()
                else -> return false
            }

            loadFragment(fragment)

            return true
        }

        private fun loadFragment(fragment: Fragment) {
            // supportFragmentManager.beginTransaction()
            //         .replace(R.id.fragment_container, fragment)
            //         .commit()
        }
    }
}
