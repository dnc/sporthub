package com.gitlab.dnc.sporthub.user.location

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.user.login.LoginActivity
import com.gitlab.dnc.sporthub.util.extensions.hasLocationAccessPermission
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import kotlinx.android.synthetic.main.activity_location.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton

class LocationActivity : AppCompatActivity() {
    private val rxObservers by lazy { CompositeDisposable() }

    private val locationRequest by lazy {
        LocationRequest().apply {
            interval = LOCATION_UPDATE_INTERVAL
            fastestInterval = LOCATION_UPDATE_INTERVAL
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private var isBoundToLocationService = false

    private lateinit var onLocationSettingsValidated: () -> Unit

    private val onLocationPermissionReceived = ::ensureValidLocationSettingsAsync

    private val locationServiceIntent by lazy { Intent(this, LocationService::class.java) }

    private val locationServiceConnection by lazy { LocationServiceConnection() }

    private val locationSettings by lazy { LocationServices.getSettingsClient(this) }

    private var handledLocationServiceIntent = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        onLocationSettingsValidated = ::startLocationService
        ensureLocationAccessPermission()

        btnStartLoginActivity.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        handleLocationServiceIntent(intent)
        handledLocationServiceIntent = true
    }

    override fun onResume() {
        super.onResume()

        if (handledLocationServiceIntent) {
            // Handled by onNewIntent, which is called right before this one
            handledLocationServiceIntent = false
            return
        }

        handleLocationServiceIntent(intent)
    }

    override fun onRestart() {
        super.onRestart()

        ensureLocationAccessPermission()
    }

    override fun onStart() {
        super.onStart()

        bindToLocationService()
    }

    override fun onStop() {
        rxObservers.clear()
        unbindFromLocationService()

        super.onStop()
    }

    override fun onDestroy() {
        rxObservers.dispose()
        unbindFromLocationService()

        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            LOCATION_SETTINGS_CHANGE_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    // User agreed to change location related settings

                    onLocationSettingsValidated()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != LOCATION_ACCESS_REQUEST_CODE) {
            return
        }

        if (grantResults.size != 2) {
            return
        }

        if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            onLocationPermissionReceived()
        } else {
            onLocationPermissionRejection()
        }
    }

    private fun handleLocationServiceIntent(intent: Intent?) {
        if (intent == null) {
            return
        }

        onLocationSettingsValidated = ::restartLocationService

        when (intent.action) {
            ACTION_REQUEST_LOCATION_PERMISSION -> {
                Log.d(TAG, "Location service requested location setting validation")

                ensureLocationAccessPermission()
            }

            ACTION_REQUEST_INTERNET_ACCESS -> {
                Log.d(TAG, "Location service requested Internet access")

                alert(getString(R.string.location_missing_internet_access_dialog_message),
                        getString(R.string.location_missing_internet_access_dialog_title)) {
                    okButton {
                        ensureLocationAccessPermission()
                    }
                }.show()
            }
        }
    }

    private fun onLocationPermissionRejection() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return
        }

        val usedDeniedTemporarily = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)

        if (usedDeniedTemporarily) {
            // The user pressed Deny, without checking the "Never ask again" checkbox
            alert(getString(R.string.location_missing_permissions_dialog_message),
                    getString(R.string.location_missing_permissions_dialog_title)) {
                okButton { ensureLocationAccessPermission() }
            }.show()
        } else {
            // The user pressed Deny, and checked the "Never ask again" checkbox
            alert(getString(R.string.location_missing_permissions_rationale_dialog_message),
                    getString(R.string.location_missing_permissions_dialog_title)) {
                okButton { openApplicationDetailsActivity() }
            }.show()
        }
    }

    private fun openApplicationDetailsActivity() {
        // https://stackoverflow.com/a/35456817
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
            data = Uri.fromParts("package", packageName, null)
        }

        startActivity(intent)
    }

    private fun ensureValidLocationSettingsAsync() {
        val locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()
        val locationSettingsResponse = locationSettings.checkLocationSettings(locationSettingsRequest)

        locationSettingsResponse.addOnSuccessListener(this) { onLocationSettingsValidated() }

        locationSettingsResponse.addOnFailureListener(this) { e ->
            if (e is ResolvableApiException) {
                e.startResolutionForResult(this@LocationActivity, LOCATION_SETTINGS_CHANGE_REQUEST_CODE)
            } else {
                if (e is ApiException) {
                    if (e.statusCode == LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE) {
                        // Airplane mode is ON

                        alert(getString(R.string.location_airplane_mode_enabled_dialog_message),
                                getString(R.string.location_airplane_mode_enabled_dialog_title)) {

                            // Recursive call, not too great. Maybe do it X types then exit the application if airplane mode is still on
                            okButton { ensureValidLocationSettingsAsync() }
                        }.show()
                    }
                } else {
                    throw e
                }
            }
        }
    }

    private fun initLocationChangeObserver(locationServiceBinder: LocationService.LocalBinder) {
        rxObservers.addAll(
                locationServiceBinder.locationChangeObserver.observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(LocationChangeObserver())
        )
    }

    private fun restartLocationService() {
        stopLocationService()
        unbindFromLocationService()

        startLocationService()
        bindToLocationService()
    }

    private fun startLocationService() {
        val intent = locationServiceIntent.apply { putExtra(LocationService.LOCATION_REQUEST_EXTRA, locationRequest) }
        startService(intent)
    }

    private fun stopLocationService() {
        unbindFromLocationService()
        stopService(locationServiceIntent)
    }

    private fun bindToLocationService() {
        isBoundToLocationService = bindService(locationServiceIntent, locationServiceConnection, 0)

        if (!isBoundToLocationService) {
            throw IllegalStateException("Couldn't bind to the location service")
        }
    }

    private fun unbindFromLocationService() {
        if (isBoundToLocationService) {
            unbindService(locationServiceConnection)
            isBoundToLocationService = false
        }
    }

    private fun ensureLocationAccessPermission() {
        if (hasLocationAccessPermission()) {
            onLocationPermissionReceived()
            return
        }

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_ACCESS_REQUEST_CODE)
    }

    companion object {
        /**
         * Value in milliseconds
         */
        private const val LOCATION_UPDATE_INTERVAL = 3000L

        private const val LOCATION_ACCESS_REQUEST_CODE = 1

        private const val LOCATION_SETTINGS_CHANGE_REQUEST_CODE = 2

        private val TAG = LocationActivity::class.simpleName

        const val ACTION_REQUEST_LOCATION_PERMISSION = "action_request_location_permission"

        const val ACTION_REQUEST_INTERNET_ACCESS = "action_request_internet_access"

        const val INTENT_HANDLED_EXTRA = "intent_handled"
    }

    private inner class LocationServiceConnection : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {

        }

        override fun onServiceConnected(name: ComponentName, service: IBinder?) {
            if (service == null) {
                return
            }

            initLocationChangeObserver(service as LocationService.LocalBinder)
        }
    }

    private inner class LocationChangeObserver : DisposableObserver<Location>() {
        override fun onNext(location: Location) {
            Log.d(TAG, "Received new location from the location service: $location")
            tvLocation.text = location.toString()
        }

        override fun onComplete() {

        }

        override fun onError(e: Throwable) {

        }
    }
}
