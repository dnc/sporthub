package com.gitlab.dnc.sporthub.user.location

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.di.GlobalDependencySingleton
import com.gitlab.dnc.sporthub.util.extensions.hasLocationAccessPermission
import com.gitlab.dnc.sporthub.util.net.InternetAvailabilityCallback
import com.gitlab.dnc.sporthub.util.net.NetworkManager
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class LocationService : Service() {
    private val localBinder by lazy { LocalBinder() }

    private val rxObservables by lazy { CompositeDisposable() }

    private val locationChangePublisher by lazy { PublishSubject.create<Location>() }

    private lateinit var locationRequest: LocationRequest

    private val locationCallback by lazy { LocationCallback() }

    private val fusedLocationProvider by lazy { LocationServices.getFusedLocationProviderClient(this) }

    private val notificationManager by lazy { GlobalDependencySingleton.notificationManager }

    private val notificationService by lazy { getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }

    private val networkManager by lazy { GlobalDependencySingleton.networkManager }

    private var isInternetAvailabilityCallbackRegistered = false

    private val monitoredNetworkTypes by lazy { arrayOf(NetworkManager.NetworkType.WIFI, NetworkManager.NetworkType.CELLULAR) }

    private val internetAvailabilityCallback by lazy { InternetAvailabilityCallback(networkManager) }

    private lateinit var handlerThread: HandlerThread

    private lateinit var handler: Handler

    override fun onCreate() {
        super.onCreate()

        showForegroundServiceNotification()

        handlerThread = HandlerThread("$CLASS_NAME-handler")
        handlerThread.start()

        handler = Handler(handlerThread.looper)
    }

    override fun onBind(intent: Intent): IBinder {
        return localBinder
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val locationRequest = intent.getParcelableExtra<LocationRequest>(LOCATION_REQUEST_EXTRA)

        if (locationRequest == null) {
            Log.w(CLASS_NAME, "LocationRequest object not specified, stopping")

            stopSelf()

            return START_NOT_STICKY
        }

        this.locationRequest = locationRequest
        startLocationListenerAsync()

        // Will get the service restarted after the process is killed (eg.: app is swiped from Overview / Recents)
        return START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        super.onDestroy()

        stopService()

        hideForegroundServiceNotification()
    }

    private fun stopService() {
        unregisterInternetAvailabilityCallback()
        stopLocationListener()
        handlerThread.quit()
        stopSelf()
    }

    private fun startLocationListenerAsync() {
        handler.post(::startLocationListener)
    }

    private fun startLocationListener() {
        networkManager.hasInternetAccessAsync({ hasInternetAccess ->
            if (hasInternetAccess) {
                if (hasLocationAccessPermission()) {
                    registerInternetAvailabilityCallback()
                    fusedLocationProvider.requestLocationUpdates(locationRequest, locationCallback, handlerThread.looper)
                } else {
                    showInvalidLocationSettingsNotification()
                    stopService()
                }
            } else {
                showMissingInternetAccessNotification()
                stopService()
            }
        }, NetworkManager.NetworkType.WIFI, NetworkManager.NetworkType.CELLULAR)
    }

    private fun stopLocationListener() {
        fusedLocationProvider.removeLocationUpdates(locationCallback)
    }

    private fun showForegroundServiceNotification() {
        val notification = getForegroundServiceNotificationBuilder().apply {
            setContentText(getString(R.string.service_location_main_notification_placeholder_text))
        }.build()

        notificationManager.createNotificationChannel(MAIN_CHANNEL_ID, getString(R.string.service_location_main_channel_name),
                getString(R.string.service_location_main_channel_description), NotificationManagerCompat.IMPORTANCE_LOW)

        startForeground(MAIN_NOTIFICATION_ID, notification)
    }

    private fun updateForegroundServiceNotification(latitude: Double, longitude: Double) {
        val notification = getForegroundServiceNotificationBuilder().apply {
            setContentText(getString(R.string.service_location_main_notification_location_info_text, latitude, longitude))
        }.build()

        notificationManager.showNotification(MAIN_NOTIFICATION_ID, notification)
    }

    private fun getForegroundServiceNotificationBuilder(): NotificationCompat.Builder {
        return NotificationCompat.Builder(this, MAIN_CHANNEL_ID)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setSmallIcon(R.drawable.ic_location_on_black_24dp)
                .setContentTitle(getString(R.string.service_location_main_notification_title))
    }

    private fun hideForegroundServiceNotification() {
        stopForeground(true)
    }

    private fun showInvalidLocationSettingsNotification() {
        notificationManager.createNotificationChannel(INVALID_LOCATION_SETTINGS_CHANNEL_ID,
                getString(R.string.service_location_invalid_location_settings_channel_name),
                getString(R.string.service_location_invalid_location_settings_channel_description),
                NotificationManagerCompat.IMPORTANCE_LOW)

        notificationService.notify(INVALID_LOCATION_SETTINGS_NOTIFICATION_ID, buildInvalidLocationSettingsNotification())
    }

    private fun buildInvalidLocationSettingsNotification(): Notification {
        val locationActivityIntent = getLocationActivityIntent().apply { action = LocationActivity.ACTION_REQUEST_LOCATION_PERMISSION }

        return NotificationCompat.Builder(this, INVALID_LOCATION_SETTINGS_CHANNEL_ID)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setSmallIcon(R.drawable.ic_location_off_black_24dp)
                .setContentTitle(getString(R.string.service_location_invalid_location_settings_notification_title))
                .setContentText(getString(R.string.service_location_invalid_location_settings_notification_text))
                .setContentIntent(PendingIntent.getActivity(this, 0, locationActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setAutoCancel(true)
                .build()
    }

    private fun showMissingInternetAccessNotification() {
        notificationManager.createNotificationChannel(MISSING_INTERNET_ACCESS_CHANNEL_ID,
                getString(R.string.service_location_missing_internet_access_channel_name),
                getString(R.string.service_location_missing_internet_access_channel_description),
                NotificationManagerCompat.IMPORTANCE_LOW)

        notificationService.notify(MISSING_INTERNET_ACCESS_NOTIFICATION_ID, buildMissingInternetAccessNotification())
    }

    private fun buildMissingInternetAccessNotification(): Notification {
        val locationActivityIntent = getLocationActivityIntent().apply { action = LocationActivity.ACTION_REQUEST_INTERNET_ACCESS }

        return NotificationCompat.Builder(this, MISSING_INTERNET_ACCESS_CHANNEL_ID)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setSmallIcon(R.drawable.ic_missing_internet_access_black_24dp)
                .setContentTitle(getString(R.string.service_location_missing_internet_access_notification_title))
                .setContentText(getString(R.string.service_location_missing_internet_access_notification_text))
                .setContentIntent(PendingIntent.getActivity(this, 0, locationActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setAutoCancel(true)
                .build()
    }

    private fun getLocationActivityIntent(): Intent {
        return Intent(this, LocationActivity::class.java)
    }

    private fun registerInternetAvailabilityCallback() {
        val networkRequest = NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR).build()

        rxObservables.addAll(
                internetAvailabilityCallback.internetAvailabilityPublisher.subscribe { isAvailable ->
                    if (isAvailable) {
                        notificationService.cancel(MISSING_INTERNET_ACCESS_NOTIFICATION_ID)
                    } else {
                        showMissingInternetAccessNotification()
                        stopService()
                    }
                }
        )

        networkManager.registerInternetAvailabilityCallback(internetAvailabilityCallback, *monitoredNetworkTypes)
        isInternetAvailabilityCallbackRegistered = true
    }

    private fun unregisterInternetAvailabilityCallback() {
        if (isInternetAvailabilityCallbackRegistered) {
            networkManager.unregisterInternetAvailabilityCallback(internetAvailabilityCallback)
            isInternetAvailabilityCallbackRegistered = false
        }
    }

    companion object {
        private const val MAIN_CHANNEL_ID = "main_channel"

        private const val INVALID_LOCATION_SETTINGS_CHANNEL_ID = "invalid_location_settings_channel"

        private const val MISSING_INTERNET_ACCESS_CHANNEL_ID = "missing_internet_access_channel"

        private const val MAIN_NOTIFICATION_ID = 1

        private const val INVALID_LOCATION_SETTINGS_NOTIFICATION_ID = 2

        private const val MISSING_INTERNET_ACCESS_NOTIFICATION_ID = 3

        private val CLASS_NAME = LocationService::class.simpleName

        private val TAG = CLASS_NAME

        const val LOCATION_REQUEST_EXTRA = "location_request_extra"
    }

    private inner class LocationCallback : com.google.android.gms.location.LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            if (locationResult == null) {
                return
            }

            Log.d(TAG, "Location result: $locationResult")

            val lastLocation = locationResult.lastLocation

            updateForegroundServiceNotification(lastLocation.latitude, lastLocation.longitude)

            locationChangePublisher.onNext(lastLocation)
        }

        override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
            if (locationAvailability == null) {
                return
            }

            Log.d(TAG, "Location is now ${if (locationAvailability.isLocationAvailable) "available" else "unavailable"}")

            if (!locationAvailability.isLocationAvailable) {
                showInvalidLocationSettingsNotification()
                stopService()
            }
        }
    }

    inner class LocalBinder : android.os.Binder() {
        val locationChangeObserver: Observable<Location> = locationChangePublisher.hide()
    }
}
