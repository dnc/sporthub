package com.gitlab.dnc.sporthub.user.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.databinding.ActivityLoginBinding
import com.gitlab.dnc.sporthub.user.model.di.DependencySingletons
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.user.recovery.PasswordRecoveryActivity
import com.gitlab.dnc.sporthub.user.registration.RegistrationActivity
import com.gitlab.dnc.sporthub.util.extensions.content
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private lateinit var loginViewModel: LoginViewModel

    private val rxObservers by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.activity = this

        val userRepository = DependencySingletons.userRepository

        loginViewModel = ViewModelProviders.of(this, LoginViewModel.Factory(userRepository)).get(LoginViewModel::class.java)


        initObservers()
        initRxObservers()
        // initCallbacks()
    }

    override fun onResume() {
        super.onResume()

        /*
        This is only necessary when the user returns to this activity from RegistrationActivity (by finishing it). It's
        not really required if this activity was just created (user clicked the launcher icon), as the [LoginViewModel]
        initializer already does this operation
         */
        loginViewModel.retrieveLastUsedUser()
    }

    override fun onDestroy() {
        rxObservers.dispose()

        super.onDestroy()
    }

    private fun initObservers() {
        loginViewModel.user.observe(this, Observer { user ->
            // bindUser(user)

            user?.let {
                applyUser(user)
            }
        })

        loginViewModel.loginOperationStatus.observe(this, Observer { loginOperationStatus ->
            when (loginOperationStatus) {
                LoginOperationStatus.IN_PROGRESS -> showLoginInitializationToast()

                LoginOperationStatus.SUCCESSFUL -> showSuccessfulLoginSnackBar()

                LoginOperationStatus.FAILED -> showFailedLoginSnackBar()
            }
        })
    }

    private fun initRxObservers() {
        rxObservers.addAll(
                btnLogin.clicks().subscribe { loginViewModel.login(collectUser()) },
                textViewRegisterHere.clicks().subscribe { startRegistrationActivity() },
                textViewRecoverHere.clicks().subscribe { startPasswordRecoveryActivity() }
        )
    }

    // private fun initCallbacks() {
    //     btnLogin.setOnClickListener {
    //         loginViewModel.login(collectUser())
    //     }
    //
    //     textViewRegisterHere.setOnClickListener {
    //         startRegistrationActivity()
    //     }
    //
    //     textViewRecoverHere.setOnClickListener {
    //         startPasswordRecoveryActivity()
    //     }
    // }

    // fun onLoginButtonClick(view: View) {
    //     loginViewModel.login(collectUser())
    //     // loginViewModel.login(collectUser()) // this called the collectUser() method that dealed with the 'binding' object
    // }
    //
    // fun onRegisterHereTextViewClick(view: View) {
    //     startRegistrationActivity()
    // }
    //
    // fun onRecoverHereTextViewClick(view: View) {
    //     startPasswordRecoveryActivity()
    // }

    private fun startRegistrationActivity() {
        val intent = Intent(this, RegistrationActivity::class.java)
        startActivity(intent)
    }

    private fun startPasswordRecoveryActivity() {
        val intent = Intent(this, PasswordRecoveryActivity::class.java)
        startActivity(intent)
    }

    private fun showLoginInitializationToast() {
        Toast.makeText(this, R.string.login_attempting_log_in, Toast.LENGTH_SHORT).show()
    }

    private fun showSuccessfulLoginSnackBar() {
        Snackbar.make(rootLayout, R.string.login_successfully_logged_in, Snackbar.LENGTH_LONG).show()
    }

    private fun showFailedLoginSnackBar() {
        Snackbar.make(rootLayout, R.string.login_failed_to_log_in, Snackbar.LENGTH_LONG).show()
    }

    private fun collectUser() = User(editTextEmail.content, editTextPassword.content)

    // private fun bindUser(user: User) {
    //     editTextEmail.setText(user.email)
    //     editTextPassword.setText(user.password)
    // }

    private fun applyUser(user: User) {
        binding.user = user
    }

    //     private fun collectUser(): User {
    //         /*
    //         This doesn't work, it simply returns the value originally assigned to binding.user.
    //
    //         Changes made to the text property of the involved EditText widgets don't affect the [User] object's properties
    //         */
    //         return binding.user!!
    //     }
}