package com.gitlab.dnc.sporthub.user.login

enum class LoginOperationStatus {
    IN_PROGRESS,
    FAILED,
    SUCCESSFUL
}