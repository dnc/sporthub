package com.gitlab.dnc.sporthub.user.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.user.model.source.UserRepository

class LoginViewModel(private val userRepository: UserRepository) : ViewModel() {
    private val _user = MutableLiveData<User>()

    private val _loginOperationStatus: MutableLiveData<LoginOperationStatus> = MutableLiveData()

    val user: LiveData<User> = _user

    val loginOperationStatus: LiveData<LoginOperationStatus> = _loginOperationStatus

    init {
        retrieveLastUsedUser()
    }

    fun login(user: User) {
        updateUser(user)

        _loginOperationStatus.value = LoginOperationStatus.IN_PROGRESS

        val existingUser = userRepository.retrieveUserByEmail(user.email)

        if (existingUser == null || existingUser.password != user.password) {
            _loginOperationStatus.value = LoginOperationStatus.FAILED
        } else {
            _loginOperationStatus.value = LoginOperationStatus.SUCCESSFUL
        }
    }

    /**
     * Makes sure the [user] [LiveData] object points to the last used [User] instance
     */
    fun retrieveLastUsedUser() {
        _user.value = userRepository.retrieveLastUsedUser()
    }

    private fun updateUser(user: User) {
        _user.postValue(user)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val userRepository: UserRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(userRepository) as T
        }
    }
}