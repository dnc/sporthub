package com.gitlab.dnc.sporthub.user.model.di

import com.gitlab.dnc.sporthub.di.GlobalDependencySingleton
import com.gitlab.dnc.sporthub.user.model.source.UserRepository
import com.gitlab.dnc.sporthub.user.model.source.local.SharedPreferencesUserSource

object DependencySingletons {
    val localUserSourceFileName by lazy { "${GlobalDependencySingleton.applicationPackage}.users" }

    val localUserSource by lazy { SharedPreferencesUserSource(GlobalDependencySingleton.applicationContext, localUserSourceFileName) }

    val userRepository by lazy { UserRepository(GlobalDependencySingleton.applicationContext, localUserSource, GlobalDependencySingleton.notificationManager) }
}