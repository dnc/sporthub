package com.gitlab.dnc.sporthub.user.model.entity

data class User(val email: String, val password: String) {
    companion object {
        private const val MIN_PASSWORD_LENGTH = 3

        private const val MAX_PASSWORD_LENGTH = 12

        fun isPasswordTooShort(password: String) = password.length < MIN_PASSWORD_LENGTH

        fun isPasswordTooLong(password: String) = password.length > MAX_PASSWORD_LENGTH

        fun isPasswordLengthValid(password: String) = !isPasswordTooShort(password) && !isPasswordTooLong(password)
    }
}