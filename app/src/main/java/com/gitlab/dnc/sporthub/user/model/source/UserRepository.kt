package com.gitlab.dnc.sporthub.user.model.source

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.util.notifications.NotificationManager
import com.gitlab.dnc.sporthub.util.receivers.ClipboardContentChangerReceiver

class UserRepository(private val context: Context, private val localUserSource: UserSource, private val notificationManager: NotificationManager) {
    fun storeUser(user: User) {
        localUserSource.saveUser(user)
    }

    fun retrieveUserByEmail(email: String): User? {
        return localUserSource.getUserByEmail(email)
    }

    fun retrieveLastUsedUser(): User? {
        return localUserSource.getLastUsedUser()
    }

    fun recoverPassword(email: String) {
        val randomlyGeneratedPassword = "123"

        localUserSource.saveUser(User(email, randomlyGeneratedPassword))

        sendPasswordRecoveryNotification(email, randomlyGeneratedPassword)
    }

    private fun sendPasswordRecoveryNotification(email: String, newPassword: String) {
        val channelName = context.getString(R.string.user_repository_password_recovery_channel_name)
        val channelDescription = context.getString(R.string.user_repository_password_recovery_channel_description)
        notificationManager.createNotificationChannel(PASSWORD_RECOVERY_CHANNEL_ID, channelName, channelDescription, NotificationManagerCompat.IMPORTANCE_HIGH)

        val notification = buildNotification(email, newPassword)
        notificationManager.showNotification(PASSWORD_RECOVERY_NOTIFICATION_ID, notification)
    }

    private fun buildNotification(email: String, newPassword: String): Notification {
        val copyNewPasswordToClipboardIntent = getClipboardContentChangerReceiverPendingIntent(newPassword)

        val notificationBuilder = NotificationCompat.Builder(context, PASSWORD_RECOVERY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon_24dp)
                .setContentTitle(context.getString(R.string.user_repository_password_recovery_notification_title))
                .setContentText(context.getString(R.string.user_repository_password_recovery_notification_content, email, newPassword))

                // For Android 7.1 and lower
                .setPriority(NotificationCompat.PRIORITY_HIGH)

                .addAction(R.drawable.ic_content_copy_black_24dp, context.getString(R.string.user_repository_copy_new_password_button_label),
                        copyNewPasswordToClipboardIntent)

        return notificationBuilder.build()
    }

    private fun getClipboardContentChangerReceiverPendingIntent(newPassword: String): PendingIntent {
        val clipboardContentChangerReceiver = ClipboardContentChangerReceiver()

        val intentFilter = IntentFilter(ClipboardContentChangerReceiver.ACTION_CHANGE_CLIPBOARD_CONTENT)
        context.registerReceiver(clipboardContentChangerReceiver, intentFilter)

        val intent = Intent(ClipboardContentChangerReceiver.ACTION_CHANGE_CLIPBOARD_CONTENT).apply {
            putExtra(ClipboardContentChangerReceiver.EXTRA_NEW_CLIPBOARD_CONTENT_LABEL,
                    context.getString(R.string.user_repository_new_password_clipboard_label))

            putExtra(ClipboardContentChangerReceiver.EXTRA_NEW_CLIPBOARD_CONTENT, newPassword)
        }

        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    companion object {
        private const val PASSWORD_RECOVERY_NOTIFICATION_ID = 1

        private const val PASSWORD_RECOVERY_CHANNEL_ID = "passwordRecoveryChannel"
    }
}