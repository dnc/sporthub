package com.gitlab.dnc.sporthub.user.model.source

import com.gitlab.dnc.sporthub.user.model.entity.User

interface UserSource {
    /**
     * Saves the given [User] instance to the data source. Should mark this [User] as the last used [User]
     * @see getLastUsedUser()
     */
    fun saveUser(user: User)

    /**
     * Attempts to retrieve the [User] instance of which [User.email] property matches the given [email]. Should mark
     * this [User] as the last used [User]
     * @see getLastUsedUser()
     * @return a [User] instance, or null if no [User] matching the given [email] is found
     */
    fun getUserByEmail(email: String): User?

    /**
     * Attempts to retrieve the last used [User] instance
     *
     * @return a [User] instance, or null if no such [User] instance is found (eg.: there aren't any saved [User] instances, because
     * both saving [saveUser] and accessing [getUserByEmail] are supposed to trigger changes on the conceptual last used [User] property)
     */
    fun getLastUsedUser(): User?
}