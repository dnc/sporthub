package com.gitlab.dnc.sporthub.user.model.source.local

import android.content.Context
import androidx.core.content.edit
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.user.model.source.UserSource

class SharedPreferencesUserSource(private val context: Context, private val sharedPreferencesFileName: String) : UserSource {
    private val sharedPreferences by lazy { context.getSharedPreferences(sharedPreferencesFileName, Context.MODE_PRIVATE) }

    override fun saveUser(user: User) {
        sharedPreferences.edit {
            putString(user.email, user.password)
        }

        setLastUsedUser(user)
    }

    override fun getUserByEmail(email: String): User? {
        val password = sharedPreferences.getString(email, null) ?: return null

        return User(email, password).also { user -> setLastUsedUser(user) }
    }

    override fun getLastUsedUser(): User? {
        val lastSavedUserEmail = sharedPreferences.getString(LAST_SAVED_USER_EMAIL_KEY, null) ?: return null

        return getUserByEmail(lastSavedUserEmail)
    }

    private fun setLastUsedUser(user: User) {
        sharedPreferences.edit {
            putString(LAST_SAVED_USER_EMAIL_KEY, user.email)
        }
    }

    companion object {
        private const val LAST_SAVED_USER_EMAIL_KEY = "lastSavedUserEmailKey"
    }
}