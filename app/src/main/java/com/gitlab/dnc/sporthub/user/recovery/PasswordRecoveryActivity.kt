package com.gitlab.dnc.sporthub.user.recovery

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.user.model.di.DependencySingletons.userRepository
import com.gitlab.dnc.sporthub.util.extensions.content
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_password_recovery.*

class PasswordRecoveryActivity : AppCompatActivity() {
    private lateinit var passwordRecoveryViewModel: PasswordRecoveryViewModel

    private val rxObservers by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_recovery)

        passwordRecoveryViewModel = ViewModelProviders.of(this, PasswordRecoveryViewModel.Factory(userRepository)).get(PasswordRecoveryViewModel::class.java)

        initObservers()
        initRxObservers()
        // initCallbacks()
    }

    override fun onDestroy() {
        rxObservers.dispose()

        super.onDestroy()
    }

    private fun initObservers() {
        passwordRecoveryViewModel.passwordRecoveryOperationStatus.observe(this, Observer { passwordRecoveryOperationStatus ->
            when (passwordRecoveryOperationStatus) {
                PasswordRecoveryOperationStatus.IN_PROGRESS -> showPasswordRecoveryInitializationToast()

                PasswordRecoveryOperationStatus.SUCCESSFUL -> showSuccessfulPasswordRecoverySnackBar()

                PasswordRecoveryOperationStatus.FAILED -> showFailedPasswordRecoverySnackBar()
            }
        })
    }

    private fun initRxObservers() {
        rxObservers.addAll(
                btnRecoverPassword.clicks().subscribe { passwordRecoveryViewModel.recoverPassword(collectRecoveryEmail()) }
        )
    }

    // private fun initCallbacks() {
    //     btnRecoverPassword.setOnClickListener {
    //         passwordRecoveryViewModel.recoverPassword(collectRecoveryEmail())
    //     }
    // }

    private fun showPasswordRecoveryInitializationToast() {
        Toast.makeText(this, R.string.recovery_attempting_password_recovery, Toast.LENGTH_SHORT).show()
    }

    private fun showSuccessfulPasswordRecoverySnackBar() {
        Snackbar.make(rootLayout, R.string.recovery_successfully_requested_password_recovery, Snackbar.LENGTH_LONG).show()
    }

    private fun showFailedPasswordRecoverySnackBar() {
        Snackbar.make(rootLayout, R.string.recovery_failed_password_recovery_request, Snackbar.LENGTH_LONG).show()
    }

    private fun collectRecoveryEmail() = editTextRecoveryEmail.content
}
