package com.gitlab.dnc.sporthub.user.recovery

enum class PasswordRecoveryOperationStatus {
    IN_PROGRESS,
    FAILED,
    SUCCESSFUL
}