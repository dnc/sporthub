package com.gitlab.dnc.sporthub.user.recovery

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.dnc.sporthub.user.model.source.UserRepository

class PasswordRecoveryViewModel(private val userRepository: UserRepository) : ViewModel() {
    private val _passwordRecoveryOperationStatus = MutableLiveData <PasswordRecoveryOperationStatus>()

    val passwordRecoveryOperationStatus = _passwordRecoveryOperationStatus

    fun recoverPassword(email: String) {
        _passwordRecoveryOperationStatus.value = PasswordRecoveryOperationStatus.IN_PROGRESS

        if (userRepository.retrieveUserByEmail(email) == null) {
            _passwordRecoveryOperationStatus.value = PasswordRecoveryOperationStatus.FAILED
            return
        }

        userRepository.recoverPassword(email)
        _passwordRecoveryOperationStatus.value = PasswordRecoveryOperationStatus.SUCCESSFUL
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val userRepository: UserRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PasswordRecoveryViewModel(userRepository) as T
        }
    }
}