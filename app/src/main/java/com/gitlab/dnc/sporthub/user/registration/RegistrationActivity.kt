package com.gitlab.dnc.sporthub.user.registration

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.user.login.LoginActivity
import com.gitlab.dnc.sporthub.user.model.di.DependencySingletons.userRepository
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.util.extensions.content
import com.gitlab.dnc.sporthub.util.extensions.isEmailAddress
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_login.editTextPassword
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.concurrent.TimeUnit

class RegistrationActivity : AppCompatActivity() {
    private lateinit var registrationViewModel: RegistrationViewModel

    private val rxObservers by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        registrationViewModel = ViewModelProviders.of(this, RegistrationViewModel.Factory(userRepository)).get(RegistrationViewModel::class.java)

        initObservers()
        initRxObservers()
        // initCallbacks()
    }

    override fun onDestroy() {
        rxObservers.dispose()

        super.onDestroy()
    }

    private fun initObservers() {
        registrationViewModel.registrationOperationResult.observe(this, Observer { registrationOperationResult ->
            when (registrationOperationResult) {
                RegistrationOperationResult.REGISTERED -> showSuccessfulRegistrationSnackBar()

                RegistrationOperationResult.EMAIL_CONFLICT -> showRegistrationConflictSnackBar()
            }
        })
    }

    private fun initRxObservers() {
        rxObservers.addAll(
                btnRegister.clicks().subscribe { registerUser() },
                textViewLogInHere.clicks().subscribe { showLoginActivity() },

                /*
                * We skip(1) because the subscriber seems to be called when the View is first initialized (even if it has no text)
                * and we don't want to display the errors immediately
                */
                editTextEmail.textChanges().skip(1).debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { updateEmailError(isEmailValid()) },

                editTextPassword.textChanges().skip(1).debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { updateInvalidPasswordLengthError() }
        )
    }

    // private fun initCallbacks() {
    //     btnRegister.setOnClickListener { registerUser() }
    //
    //     textViewLogInHere.setOnClickListener { showLoginActivity() }
    // }

    private fun showSuccessfulRegistrationSnackBar() {
        Snackbar.make(rootLayout, R.string.registration_successfully_registered, Snackbar.LENGTH_INDEFINITE)
                .apply {
                    setAction(R.string.registration_successful_registration_action_label) { showLoginActivity() }
                }
                .show()
    }

    private fun showRegistrationConflictSnackBar() {
        Snackbar.make(rootLayout, R.string.registration_email_in_use, Snackbar.LENGTH_LONG).show()
    }

    private fun registerUser() {
        val emailIsValid = isEmailValid()
        updateEmailError(emailIsValid)

        if (!emailIsValid) {
            return
        }

        val passwordsMatch = doPasswordsMatch()
        updateConfirmationPasswordError(passwordsMatch)

        if (!passwordsMatch) {
            return
        }

        val passwordLengthIsValid = isPasswordLengthValid()
        updateInvalidPasswordLengthError()

        if (!passwordLengthIsValid) {
            return
        }

        registrationViewModel.register(collectUser())
    }

    private fun collectUser() = User(editTextEmail.content, collectPassword())

    private fun collectPassword() = editTextPassword.content

    private fun updateEmailError(emailIsValid: Boolean) {
        if (emailIsValid) {
            tilEmail.error = null
        } else {
            tilEmail.error = getString(R.string.registration_invalid_email)
        }
    }

    private fun updateInvalidPasswordLengthError() {
        val password = collectPassword()

        when {
            User.isPasswordTooLong(password) -> tilPassword.error = getString(R.string.registration_password_too_long)
            User.isPasswordTooShort(password) -> tilPassword.error = getString(R.string.registration_password_too_short)
            else -> tilPassword.error = null
        }
    }

    private fun updateConfirmationPasswordError(passwordsMatch: Boolean) {
        if (passwordsMatch) {
            tilPasswordConfirmation.error = null
        } else {
            tilPasswordConfirmation.error = getString(R.string.registration_passwords_do_not_match)
        }
    }

    private fun isEmailValid() = editTextEmail.content.isEmailAddress()

    private fun doPasswordsMatch() = editTextPassword.content == editTextPasswordConfirmation.content

    private fun isPasswordLengthValid() = User.isPasswordLengthValid(collectPassword())

    private fun showLoginActivity() {
        /*
            If the user somehow reached this activity directly, there's no existing Login activity instance go back to,
            so create a new one. See: https://stackoverflow.com/a/15664268
         */
        if (isTaskRoot) {
            startLoginActivity()
        } else {
            super.onBackPressed()
        }
    }

    private fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}
