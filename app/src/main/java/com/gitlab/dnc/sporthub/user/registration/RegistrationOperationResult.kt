package com.gitlab.dnc.sporthub.user.registration

enum class RegistrationOperationResult {
    REGISTERED,
    EMAIL_CONFLICT
}