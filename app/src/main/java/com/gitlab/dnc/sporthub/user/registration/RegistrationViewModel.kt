package com.gitlab.dnc.sporthub.user.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.dnc.sporthub.user.model.entity.User
import com.gitlab.dnc.sporthub.user.model.source.UserRepository

class RegistrationViewModel(private val userRepository: UserRepository) : ViewModel() {
    private val _user: MutableLiveData<User> = MutableLiveData()

    private val _registrationOperationResult: MutableLiveData<RegistrationOperationResult> = MutableLiveData()

    val user: LiveData<User> = _user

    val registrationOperationResult: LiveData<RegistrationOperationResult> = _registrationOperationResult

    fun register(user: User) {
        updateUser(user)

        _registrationOperationResult.value = null

        val existingUser = userRepository.retrieveUserByEmail(user.email)

        if (existingUser != null) {
            _registrationOperationResult.value = RegistrationOperationResult.EMAIL_CONFLICT
            return
        }

        userRepository.storeUser(user)
        _registrationOperationResult.value = RegistrationOperationResult.REGISTERED
    }

    private fun updateUser(user: User) {
        _user.postValue(user)
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val userRepository: UserRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return RegistrationViewModel(userRepository) as T
        }
    }
}