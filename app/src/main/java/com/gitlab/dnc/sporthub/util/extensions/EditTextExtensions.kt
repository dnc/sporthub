package com.gitlab.dnc.sporthub.util.extensions

import android.widget.EditText

val EditText.content: String
    get() = this.text.toString()