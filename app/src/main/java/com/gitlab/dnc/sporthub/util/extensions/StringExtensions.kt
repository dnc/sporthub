package com.gitlab.dnc.sporthub.util.extensions

import android.util.Patterns

fun String?.isEmailAddress(): Boolean {
    return !this.isNullOrBlank() && this.matches(Patterns.EMAIL_ADDRESS.toRegex())
}