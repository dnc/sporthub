package com.gitlab.dnc.sporthub.util.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.inflate(@LayoutRes layoutResourceId: Int): View {
    return LayoutInflater.from(this.context).inflate(layoutResourceId, this, false)
}