package com.gitlab.dnc.sporthub.util.net

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import android.net.Network
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class InternetAvailabilityCallback(private val networkManager: NetworkManager) : ConnectivityManager.NetworkCallback() {
    private val _internetAvailabilityPublisher by lazy { PublishSubject.create<Boolean>() }

    val internetAvailabilityPublisher: Observable<Boolean> = _internetAvailabilityPublisher.hide()

    override fun onLost(network: Network) {
        handleInternetAvailabilityChange(false)
    }

    override fun onAvailable(network: Network) {
        handleInternetAvailabilityChange(true)
    }

    @SuppressLint("CheckResult")
    private fun handleInternetAvailabilityChange(isAvailable: Boolean) {
        if (!isAvailable) {
            _internetAvailabilityPublisher.onNext(false)
            return
        }

        networkManager.getInternetConnectionValidator().subscribe { internetIsAvailable ->
            _internetAvailabilityPublisher.onNext(internetIsAvailable)
        }
    }
}