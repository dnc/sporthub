package com.gitlab.dnc.sporthub.util.net

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.annotation.RequiresApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

typealias InternetAccessCallback = (Boolean) -> Unit

class NetworkManager(private val application: Application) {
    private val connectivityManager by lazy { application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }

    @SuppressLint("CheckResult")
    fun hasInternetAccessAsync(internetAccessCallback: InternetAccessCallback, vararg networkTypes: NetworkType = arrayOf(NetworkType.WIFI, NetworkType.CELLULAR)) {
        if (!hasInternetAccess(networkTypes)) {
            internetAccessCallback(false)
            return
        }

        getInternetConnectionValidator().subscribe { internetIsAvailable ->
            internetAccessCallback(internetIsAvailable)
        }
    }

    fun registerInternetAvailabilityCallback(internetAvailabilityCallback: InternetAvailabilityCallback, vararg networkTypes: NetworkType) {
        connectivityManager.registerNetworkCallback(buildNetworkRequest(networkTypes.toSortedSet()), internetAvailabilityCallback)
    }

    fun unregisterInternetAvailabilityCallback(internetAvailabilityCallback: InternetAvailabilityCallback) {
        connectivityManager.unregisterNetworkCallback(internetAvailabilityCallback)
    }

    fun getInternetConnectionValidator(): Single<Boolean> {
        return Single.create<Boolean> { emitter ->
            val url = URL("https://google.com")

            try {
                with(url.openConnection() as HttpURLConnection) {
                    connectTimeout = PING_TIMEOUT
                    connect()
                    disconnect()

                    emitter.onSuccess(true)
                }
            } catch (e: IOException) {
                emitter.onSuccess(false)
            }
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    private fun buildNetworkRequest(networkTypes: Set<NetworkType>): NetworkRequest {
        val networkRequestBuilder = NetworkRequest.Builder()

        for (networkType in networkTypes) {
            val transportType = when (networkType) {
                NetworkType.WIFI -> NetworkCapabilities.TRANSPORT_WIFI
                NetworkType.CELLULAR -> NetworkCapabilities.TRANSPORT_CELLULAR
            }

            networkRequestBuilder.addTransportType(transportType)
        }

        return networkRequestBuilder.build()
    }

    private fun hasInternetAccess(networkTypes: Array<out NetworkType>): Boolean {
        val uniqueNetworkTypes = networkTypes.toSortedSet()

        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            hasInternetAccess22(uniqueNetworkTypes)
        } else {
            hasInternetAccess23(uniqueNetworkTypes)
        }
    }

    private fun hasInternetAccess22(networkTypes: Set<NetworkType>): Boolean {
        val activeNetworkInfo = connectivityManager.activeNetworkInfo ?: return false

        activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI || activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE

        var flag = false

        for (networkType in networkTypes) {
            val type = when (networkType) {
                NetworkType.WIFI -> ConnectivityManager.TYPE_WIFI
                NetworkType.CELLULAR -> ConnectivityManager.TYPE_MOBILE
            }

            flag = flag || activeNetworkInfo.type == type
        }

        return flag
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun hasInternetAccess23(networkTypes: Set<NetworkType>): Boolean {
        val networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                ?: return false

        var flag = false

        for (networkType in networkTypes) {
            val type = when (networkType) {
                NetworkType.WIFI -> NetworkCapabilities.TRANSPORT_WIFI
                NetworkType.CELLULAR -> NetworkCapabilities.TRANSPORT_CELLULAR
            }

            flag = flag || networkCapabilities.hasTransport(type)
        }

        return flag
    }

    companion object {
        private const val PING_TIMEOUT = 3000
    }

    enum class NetworkType {
        WIFI,
        CELLULAR
    }
}