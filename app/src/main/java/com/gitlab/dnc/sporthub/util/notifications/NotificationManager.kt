package com.gitlab.dnc.sporthub.util.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.gitlab.dnc.sporthub.R

class NotificationManager(private val context: Context) {
    private val notificationManager by lazy { context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }

    fun createNotificationChannel(id: String, name: String, description: String, importance: Int) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        val channel = NotificationChannel(id, name, importance).apply { this.description = description }
        notificationManager.createNotificationChannel(channel)
    }

    fun buildNotification(channelId: String, title: String, content: String): Notification {
        return NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_notification_icon_24dp)
                .setContentTitle(title)
                .setContentText(content)
                .build()
    }

    fun showNotification(id: Int, notification: Notification) {
        notificationManager.notify(id, notification)
    }
}