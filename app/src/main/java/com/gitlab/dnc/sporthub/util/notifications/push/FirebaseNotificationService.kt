package com.gitlab.dnc.sporthub.util.notifications.push

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseNotificationService : FirebaseMessagingService() {
    companion object {
        private val tag = FirebaseNotificationService::class.simpleName!!
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)

        Log.d(tag, "Device's firebase token: $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage.data == null) {
            Log.d(tag, "Ignoring data-less notification")
            return
        }

        val notificationType = remoteMessage.data["notificationType"]

        if (notificationType == null) {
            Log.d(tag, "Ignoring notification without a specified type")
            return
        }

        val sanitizedData = remoteMessage.data.filter { it.key != null }

        val notification = NotificationFactory.of(notificationType)
        notification.handle(sanitizedData)
    }
}