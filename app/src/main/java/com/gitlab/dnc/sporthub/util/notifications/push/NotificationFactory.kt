package com.gitlab.dnc.sporthub.util.notifications.push

import com.gitlab.dnc.sporthub.util.notifications.push.types.AlertNotification
import com.gitlab.dnc.sporthub.util.notifications.push.types.ChatNotification
import com.gitlab.dnc.sporthub.util.notifications.push.types.LocationNotification
import com.gitlab.dnc.sporthub.util.notifications.push.types.TimerNotification
import java.util.*

object NotificationFactory {
    fun of(notificationType: String): PushNotification {
        return when (val lowercaseNotificationType = notificationType.toLowerCase(Locale.US)) {
            "chat" -> ChatNotification()
            "alert" -> AlertNotification()
            "timer" -> TimerNotification()
            "location" -> LocationNotification()
            else -> throw InvalidNotificationTypeException(lowercaseNotificationType)
        }
    }

    class InvalidNotificationTypeException(type: String) : RuntimeException("Invalid notification type '$type'")
}