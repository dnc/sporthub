package com.gitlab.dnc.sporthub.util.notifications.push

import androidx.annotation.StringRes
import com.gitlab.dnc.sporthub.di.GlobalDependencySingleton

abstract class PushNotification {
    val applicationContext = GlobalDependencySingleton.applicationContext

    val notificationManager = GlobalDependencySingleton.notificationManager

    fun getStringResource(@StringRes id: Int): String {
        return applicationContext.getString(id)
    }

    abstract fun handle(data: Map<String, String>)
}