package com.gitlab.dnc.sporthub.util.notifications.push.types

import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.util.notifications.push.PushNotification

class AlertNotification : PushNotification() {
    init {
        notificationManager.createNotificationChannel(NOTIFICATION_CHANNEL_ID,
                getStringResource(R.string.alert_notification_channel_name),
                getStringResource(R.string.alert_notification_channel_description),
                NotificationManagerCompat.IMPORTANCE_HIGH)
    }

    override fun handle(data: Map<String, String>) {
        val alertMessage = data["alertMessage"] ?: ""

        notificationManager.showNotification(NOTIFICATION_ID,
                notificationManager.buildNotification(NOTIFICATION_CHANNEL_ID, getStringResource(R.string.alert_notification_title), alertMessage)
        )
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "alertChannel"

        private const val NOTIFICATION_ID = 1111
    }
}