package com.gitlab.dnc.sporthub.util.notifications.push.types

import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.util.notifications.push.PushNotification

class ChatNotification : PushNotification() {
    init {
        notificationManager.createNotificationChannel(NOTIFICATION_CHANNEL_ID,
                getStringResource(R.string.chat_notification_channel_name),
                getStringResource(R.string.chat_notification_channel_description),
                NotificationManagerCompat.IMPORTANCE_DEFAULT)
    }

    override fun handle(data: Map<String, String>) {
        val senderUsername = data["senderUsername"] ?: return
        val message = data["message"] ?: ""

        notificationManager.showNotification(NOTIFICATION_ID,
                notificationManager.buildNotification(NOTIFICATION_CHANNEL_ID,
                        applicationContext.getString(R.string.chat_notification_title, senderUsername), message)
        )
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "chatChannel"

        private const val NOTIFICATION_ID = 2222
    }
}