package com.gitlab.dnc.sporthub.util.notifications.push.types

import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.util.notifications.push.PushNotification

class LocationNotification : PushNotification() {
    init {
        notificationManager.createNotificationChannel(NOTIFICATION_CHANNEL_ID,
                getStringResource(R.string.location_notification_channel_name),
                getStringResource(R.string.location_notification_channel_description),
                NotificationManagerCompat.IMPORTANCE_DEFAULT)
    }

    override fun handle(data: Map<String, String>) {
        val latitude = data["latitude"]?.toDouble() ?: return
        val longitude = data["longitude"]?.toDouble() ?: return

        notificationManager.showNotification(NOTIFICATION_ID,
                notificationManager.buildNotification(NOTIFICATION_CHANNEL_ID,
                        getStringResource(R.string.location_notification_title),
                        applicationContext.getString(R.string.location_notification_content, latitude, longitude))
        )
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "locationChannel"

        private const val NOTIFICATION_ID = 3333
    }
}