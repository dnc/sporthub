package com.gitlab.dnc.sporthub.util.notifications.push.types

import androidx.core.app.NotificationManagerCompat
import com.gitlab.dnc.sporthub.R
import com.gitlab.dnc.sporthub.util.notifications.push.PushNotification

class TimerNotification : PushNotification() {
    init {
        notificationManager.createNotificationChannel(NOTIFICATION_CHANNEL_ID,
                getStringResource(R.string.timer_notification_channel_name),
                getStringResource(R.string.timer_notification_channel_description),
                NotificationManagerCompat.IMPORTANCE_DEFAULT)
    }

    override fun handle(data: Map<String, String>) {
        val timerMessage = data["timerMessage"] ?: ""

        notificationManager.showNotification(NOTIFICATION_ID,
                notificationManager.buildNotification(NOTIFICATION_CHANNEL_ID, getStringResource(R.string.timer_notification_title), timerMessage)
        )
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "timerChannel"

        private const val NOTIFICATION_ID = 4444
    }
}