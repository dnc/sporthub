package com.gitlab.dnc.sporthub.util.receivers

import android.content.*

class ClipboardContentChangerReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != ACTION_CHANGE_CLIPBOARD_CONTENT) {
            return
        }

        val newClipboardContentLabel = intent.getStringExtra(EXTRA_NEW_CLIPBOARD_CONTENT_LABEL) ?: return
        val newClipboardContent = intent.getStringExtra(EXTRA_NEW_CLIPBOARD_CONTENT) ?: return

        val clipData = ClipData.newPlainText(newClipboardContentLabel, newClipboardContent)
        getClipboardManager(context).setPrimaryClip(clipData)
    }

    private fun getClipboardManager(context: Context): ClipboardManager {
        return context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    companion object {
        const val EXTRA_NEW_CLIPBOARD_CONTENT_LABEL = "newClipboardContentLabel"

        const val EXTRA_NEW_CLIPBOARD_CONTENT = "newClipboardContent"

        const val ACTION_CHANGE_CLIPBOARD_CONTENT = "changeClipboardContent"
    }
}